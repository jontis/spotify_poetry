#!/usr/bin/env python3.3

# read in text in one format and deliver a spotify playlist, consisting of song titles that resemble the message.

# import core libraries
import os
import sys
import threading
import time

# import draft_to_pl_class class
import draft_to_pl_class

# parse command line for filename
if (len(sys.argv) > 1):
    print(len(sys.argv))
    filename = sys.argv[1]
else:
    filename = 'example.txt'
operations_file = open(filename)
draft_and_pl = draft_to_pl_class.DraftToPL()

# read in the text
for line in operations_file:
    draft_and_pl.draft_append_row(line)
operations_file.close()


draft_and_pl.draft_display()

# generate query space
draft_and_pl.query_space_generate(5) # call with maxwords of query space to be built


# make song database and check local information
spotify_songs = draft_to_pl_class.SpotifySongs(draft_and_pl.get_query_space())

# spawn a daemon thread that queries spotify's api
def query_thread():
    print('I spawned a query daemon')
    spotify_songs.multi_thread_query()

query_daemon_thread = threading.Thread(target=query_thread,  daemon=True)
query_daemon_thread.start()

# check if the local song database is growing
for i in range(0, 60):
    if spotify_songs.local_database_size() > 0:
        keyword_matcher = draft_to_pl_class.KeyWordMatcher(draft_and_pl.draft_get(), spotify_songs.local_database_get())
        print('queries made: ' + str(spotify_songs.local_database_size()) + ', usable elements: ' + str(keyword_matcher.get_element_size()))
        if keyword_matcher.get_element_size() > 0:
            keyword_matcher.match()  
            print('solution: ', keyword_matcher.get_solution())
    time.sleep(1)

# print(spotify_songs.local_database_keys())

# create playlist from solution
playlist_writer = draft_to_pl_class.PlWriter(keyword_matcher.get_solution(), keyword_matcher.get_trackIDs())
playlist_writer.generate_html_playlist()
print('wrote playlist.html')