import os
import io
import urllib.request
import urllib.parse
import urllib.error
import json
import time
import pickle
import threading

class DraftToPL:
    """draft to playlist"""
    def __init__(self):
        self.draft = list()    
    
    def draft_append_row(self, row):
        # clean input before appending, now only cleaning out \n and ,
        if row.strip() != '':
            self.draft.append(row.strip().replace(',', '').lower())
    
    def draft_get(self):
        return self.draft    
    
    def draft_display(self):
        print(self.draft)
    
    def query_space_generate(self, max_words):
        query_space = set() # create holding variable of uniques
        for N in range(1, max_words+1):
            for line in self.draft:
                for i in range(0, len(line.split()) - N +1):
                    query_space.add(' '.join(line.split()[i: i+N]))
        self.query_space = query_space
    
    def get_query_space(self):
        return self.query_space
    

class SpotifySongs:
    """new or saved queries"""
    def __init__(self, songs_space):
        self.songs_pending_query = list()
        # check for existent query database
        if os.path.isfile('spotify_database.txt'):
            file = open('spotify_database.txt', 'rb')
            self.songs_local = pickle.load(file) # load data base
            file.close()
            print('loaded local database with ' + str(self.local_database_size()) + ' keys')
        else:
            self.songs_local = {}
            print('no local database found, starting a new one')

        
        # check which information is local and what needs to be queried from Spotify
        for song in songs_space:
            if not self.known_song(song):
                self.songs_pending_query.append(song)
    
    def local_database_size(self):
        return len(self.songs_local)
    
    def local_database_get(self):
        return self.songs_local
    
    def local_database_keys(self):
        return self.songs_local.keys()

    def known_song(self, song):
        if song in self.songs_local:
            return True
        else:
            return False
    

    def query_spotify(self, song):
        urlstring = 'http://ws.spotify.com/search/1/track.json?q=' + song.replace(' ', '+')
        req = urllib.request.Request(urlstring)
        try: 
            response = urllib.request.urlopen(req)
        except urllib.error.URLError as e:
            print(e.reason)
            if hasattr(e, 'reason'):
                print('We failed to reach a server.')
                print('Reason: ', e.reason)
            elif hasattr(e, 'code'):
                print('The server couldn\'t fulfill the request.')
                print('Error code: ', e.code)
        else:
            jsonResponse = json.loads(response.read().decode('utf-8'))
            if 'tracks' in jsonResponse:
                track_set = jsonResponse['tracks']
                while len(track_set) > 0:
                    track = track_set.pop() # pop until we find a track title that matches song
                    if track['name'].lower() == song.lower():
                        return {song: track}
        return {song: ''}
    
    def query_worker(self, song):
        song_result = self.query_spotify(song)
        self.songs_local.update(song_result) #result, does this go to global?

    
    def multi_thread_query(self):
        # multi-thread away with pending songs, start by dumbing it to 5 / sec (api limit is 10)
        # no error handling at first
        
        threads = []
        while len(self.songs_pending_query) > 0:
            # spawn thread
            song = self.songs_pending_query.pop()
            # print(song)
            t = threading.Thread(target=self.query_worker, kwargs={'song': song}, daemon=True)
            threads.append(t)
            t.start()
            time.sleep(.2) # sleep to respect the api limitation
        self.persist_local_database() # persist the extended database 

    
    
    def persist_local_database(self):
        # save data base locally
        time.sleep(5) # dumb wait to let the last queries get their reply before writing database
        file = open('spotify_database.txt', 'wb')
        pickle.dump(self.songs_local, file)
        file.close()
        print('done with queries, wrote database')
    

class KeyWordMatcher:
    def __init__(self, draft, local_songs):
        self.draft = draft
        # purge the unavailable
        self.elements_database = dict((k, v) for k, v in local_songs.items() if v)
        self.elements = list(self.elements_database.keys())
    
    def get_element_size(self):
        return len(self.elements)
    
    def match(self):
        self.solution = list()
        for row in self.draft:
            self.solution.append(self.match_row(row.split()))
    
    def match_row(self, row):
        self.row_solutions = []
        self.match_element([], row) # will fill row_solutions with all unique solutions
        # find the one with lowest score
        best_solution = len(row) * [''] # init to worst possible, not a word matched 
        best_score = self.score_row(best_solution)
        for row_solution in self.row_solutions:
            score_i = self.score_row(row_solution)
            if score_i < best_score:
                best_score = score_i
                best_solution = row_solution
        return best_solution
    
    def score_row(self, row_solution):
        # scoring: lowest score wins, give 1 for every track, 2 for every missed word in draft
        # tune for preference
        track_penalty = 1
        missed_word_penalty = 2
        n_missed = row_solution.count('')
        n_tracks = len(row_solution) - n_missed
        return (n_tracks * track_penalty + n_missed * missed_word_penalty)
    
    
    def match_element(self, matched, remaining):
        for test_element in self.elements:
            if len(remaining) >= len(test_element.split()):
                if remaining[:len(test_element.split())] == test_element.split():
                    if len(remaining) == len(test_element.split()):
                        self.row_solutions.append(matched + [test_element])
                    else:
                        self.match_element(matched + [test_element], remaining[len(test_element.split()):])
        if len(remaining) == 1:
            self.row_solutions.append(matched + ['']) # failed match, append '' for missed word
        else:
            self.match_element(matched + [''], remaining[1:])
    
    def get_solution(self):
        return self.solution
    
    def get_trackIDs(self):
        trackIDs = {}
        for element in self.elements:
            trackIDs.update({element: self.elements_database[element]['href'].split(':')[2]}) # get the spotify track ID
        return trackIDs
    
class PlWriter:
    def __init__(self, solution, trackIDs):
        self.solution = solution
        self.trackIDs = trackIDs
        self.top = '<!DOCTYPE html><html><body><ul>'
        self.bottom = '</ul></body></html>'
    
    def list_item(self, title, trackID):
        title = title[:1].capitalize() + title[1:]
        return '<li><a href="http://open.spotify.com/track/' + trackID + '">' + title + '</a></li>'
        
    def generate_html_playlist(self):
        html_file = open('playlist.html', 'w')
        html_file.write(self.top)
        for row in self.solution:
            for title in row:
                if title != '': html_file.write(self.list_item(title,  self.trackIDs[title]))
        html_file.write(self.bottom)
        html_file.close()
